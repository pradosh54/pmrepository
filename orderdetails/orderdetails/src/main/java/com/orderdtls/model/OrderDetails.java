package com.orderdtls.model;

import java.util.Date;
import java.util.List;

public abstract class OrderDetails {
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public List<ItemDetails> getItemsDtls() {
		return itemsDtls;
	}
	public void setItemsDtls(List<ItemDetails> itemsDtls) {
		this.itemsDtls = itemsDtls;
	}
	
	
	private int orderId; 
	private Date orderDate; 
	private String orderStatus; 
	private List<ItemDetails> itemsDtls; 
	
}
