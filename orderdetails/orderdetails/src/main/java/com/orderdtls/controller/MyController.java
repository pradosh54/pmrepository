package com.orderdtls.controller;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.orderdtls.model.OrderDetails;
import com.orderdtls.service.OrderService;
import com.orderdtls.service.OrderServiceImpl;



@RestController
public class MyController {

    @Autowired
    private OrderService ordService;

    @Bean
    public OrderServiceImpl getOrderService() {
       return new OrderServiceImpl();   
    }

    @RequestMapping(value = "/order/create", method = RequestMethod.POST)
    public List<String> findString(@RequestBody String reqPayload) {
    	
    	//String searchType = headers.get("searchtype");
    	System.out.println("inside getsearchvalue method "+ reqPayload);
    	ordService.createOrder(reqPayload);
    	List<String> retValue = new ArrayList<String>(); 
    	retValue.add("Order created Successfully");

    	return retValue;
    }
    

    
    @RequestMapping("/order/{id}")
    public Object findOrderDetails(@PathVariable  String id) {
    	
    	System.out.println("Order id is:-"+ id);
    	HashMap<String, Object> retValue = ordService.getOderDetails(Integer.parseInt(id)); 

    	return retValue;
    }
    
    

    @RequestMapping("/orders")
    public Object findAllOrderDetails() {
    	
    	List<HashMap> retValue = ordService.getAllOders(); 
    	
    	return retValue;
    }
}
