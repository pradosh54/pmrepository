package com.orderdtls.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.orderdtls.model.ItemDetails;
import com.orderdtls.model.OrderDetails;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
 
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;


public class OrderServiceImpl implements OrderService{

    @Autowired
    private JdbcTemplate jtm;

	
	@Override
	public String createOrder(String pPayload)  {
		int orderId = 0; 
		System.out.println("inside createOrder field :"+ pPayload);
		JsonElement jsonTree = new JsonParser().parse(pPayload);
		System.out.println("jsonTree: "+ jsonTree);
		 if(jsonTree != null && jsonTree.isJsonArray()) {
			 JsonArray jsonArray = jsonTree.getAsJsonArray();
			 for(int i =0; i< jsonArray.size(); i++) {
				 JsonObject jsonOrdDtls = jsonArray.get(i).getAsJsonObject();
				 System.out.println("Order date : "+ jsonOrdDtls.get("orderDate").toString());
				 JsonElement orderDate = jsonOrdDtls.get("orderDate"); 
				 // this will insert the order record in the order table and return its order id 
				 orderId = insertOrderDtls(orderDate.getAsString()); 
				 
				 JsonArray itemsDtls = jsonOrdDtls.get("items").getAsJsonArray();
				 for(int j = 0; j<itemsDtls.size(); j ++) {
					 JsonObject itemDtls = itemsDtls.get(j).getAsJsonObject();
					 JsonElement itemName = itemDtls.get("itemName");
					 JsonElement itemUnitPrice = itemDtls.get("itemUnitPrice");
					 JsonElement itemQuantity = itemDtls.get("itemQuantity");
					 int itemId = findItem(itemName.getAsString(), itemUnitPrice.getAsString());
					 // insert in tblOrderItem tables 
					 insertInTblOrderItem(orderId,itemId, itemQuantity.getAsInt() ); 
					 
				 }
				 
			 }
		    }		
	
	return null; 
	}
	

	private void insertInTblOrderItem(int orderId, int itemId, int asInt) {
		String sqlInsert = "INSERT INTO tblOrderItem (orderId, itemId, itemQuantity)"
				+ " VALUES (?, ?, ?)";
		System.out.println("sqlInsert:-"+sqlInsert+" orderId:-"+ orderId+"  itemId:--"+itemId +"  asInt :--"+ asInt );
		jtm.update(sqlInsert, orderId, itemId, asInt );
		
	}


	private int insertOrderDtls(String pDate) {
		int maxOrderCount = getMaxOrderCount();
		maxOrderCount++;
		String ordStatus = "Initiated"; 
		
		String sqlInsert = "INSERT INTO tblOrder (orderId, orderDate, orderStatus)"
				+ " VALUES (?, ?, ?)";
		System.out.println("sqlInsert:-"+sqlInsert+" maxOrderCount:-"+ maxOrderCount+"  pDate:--"+pDate +"  ordStatus :--"+ ordStatus );
		jtm.update(sqlInsert, maxOrderCount, pDate, ordStatus );
		return maxOrderCount;
	}

	private int getMaxOrderCount() {
		int maxOrderId = 0 ; 
        String sql = "SELECT Max(orderId) as maxorderid FROM tblOrder";
        System.out.println("sql:--"+ sql );
        

        maxOrderId = jtm.queryForObject(sql,  new RowMapper<Integer>() {
                 public Integer mapRow(ResultSet result, int rowNum) throws SQLException {
                	return result.getInt("maxorderid");
                 }
        });
        return maxOrderId;
	}


	private int findItem(String itemName, String itemUnitPrice) {
		
        
        String finalStrSearch =  itemName.toLowerCase().trim() ;
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("ItemName", itemName);
        String sql = "SELECT * FROM tblItem WHERE lower(itemName) = ?";
        System.out.println("sql:--"+ sql + " \n finalStrSearch:--"+finalStrSearch);
        

        ItemDetails ItemVal = jtm.queryForObject(sql,  new RowMapper<ItemDetails>() {
                 public ItemDetails mapRow(ResultSet result, int rowNum) throws SQLException {
        	 		ItemDetails itemDtls = new ItemDetails();
        	 		itemDtls.setItemid(result.getInt("itemId"));
        	 		itemDtls.setItemName(result.getString("itemName"));
        	 		itemDtls.setItemUnitPrice(result.getInt("itemUnitPrice"));
        	 		itemDtls.setItemQuantity(0);
                    return itemDtls;
                 }
        }, new Object[] { itemName.toLowerCase().trim() });
        
        System.out.println("ItemVal:"+ ItemVal.toString());
        
        int itemId = 0 ; 
        if(ItemVal!=null ) {
        	itemId = ItemVal.getItemid();
        }
        
		return itemId;
	}

	@Override
	public HashMap<String, Object> getOderDetails(int orderId) {
		HashMap<String, Object> hMapDateStatus = getDateNStatusOfOrder(orderId);
		hMapDateStatus.put("orderId", ""+orderId);
		List<HashMap> itemsDtls = findItemsInOrder(orderId);
		hMapDateStatus.put("items", itemsDtls); 
		return hMapDateStatus;
	}

	private List<HashMap> findItemsInOrder(int orderId) {
        String sql = "SELECT * FROM tblOrderItem WHERE orderId = "+ orderId;
        List<HashMap<String, Object>> lstHashMpa = new ArrayList(); 
        List<HashMap> ItemVal =  jtm.query(sql,   new RowMapper<HashMap>() {
                 public HashMap mapRow(ResultSet result, int rowNum) throws SQLException {
                	HashMap<String, Object> retHashMap = new HashMap<String, Object>();
                	String itemId =  result.getString("itemId"); 
					retHashMap.put("itemId", itemId); 
        	 		retHashMap.put("itemQuantity", result.getString("itemQuantity")); 
        	 		HashMap<String, String> hMapPriceName = getNamePriceOfItem(itemId); 
        	 		retHashMap.put("itemName", hMapPriceName.get("itemName"));
        	 		retHashMap.put("itemUnitPrice", hMapPriceName.get("itemUnitPrice"));
        	 		return (HashMap) retHashMap; 
                 }

				private HashMap<String, String> getNamePriceOfItem(String itemId) {
			        String sql = "SELECT * FROM tblItem WHERE itemId = ?";

			        HashMap<String, String> ItemVal = jtm.queryForObject(sql,  new RowMapper<HashMap>() {
			                 public HashMap mapRow(ResultSet result, int rowNum) throws SQLException {
			        	 		HashMap<String, String> retHashMap = new HashMap<String, String>();
								retHashMap.put("itemName", result.getString("itemName")); 
			        	 		retHashMap.put("itemUnitPrice", result.getString("itemUnitPrice")); 
			                    return retHashMap;
			                 }
			        }, new Object[] { orderId });
			        System.out.println("ItemVal:"+ ItemVal.toString());
					return ItemVal;
				}
        });
        System.out.println("ItemVal:"+ ItemVal.toString());
		return ItemVal;
	}


	private HashMap<String, Object> getDateNStatusOfOrder(int orderId) {
        String sql = "SELECT * FROM tblOrder WHERE orderId = ?";

        HashMap<String, Object> ItemVal = jtm.queryForObject(sql,  new RowMapper<HashMap>() {
                 public HashMap mapRow(ResultSet result, int rowNum) throws SQLException {
        	 		HashMap<String, String> retHashMap = new HashMap<String, String>();
					retHashMap.put("orderDate", result.getString("orderDate")); 
        	 		retHashMap.put("orderStatus", result.getString("orderStatus")); 
                    return retHashMap;
                 }
        }, new Object[] { orderId });
        System.out.println("ItemVal:"+ ItemVal.toString());
		return ItemVal;
	}


	
	@Override
	public List<HashMap> getAllOders() {
        String sql = "SELECT * FROM tblOrder";
        
        List<HashMap> ItemVal =  jtm.query(sql,   new RowMapper<HashMap>() {
                 public HashMap mapRow(ResultSet result, int rowNum) throws SQLException {
                	HashMap<String, Object> retHashMap = new HashMap<String, Object>();
                	String orderId =  result.getString("orderId"); 
                	retHashMap = getOderDetails(Integer.parseInt(orderId));
                	return (HashMap) retHashMap; 
                 }
        });
        System.out.println("ItemVal:"+ ItemVal.toString());
		return ItemVal;

	}

}
