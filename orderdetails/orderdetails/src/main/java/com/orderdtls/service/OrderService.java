package com.orderdtls.service;

import java.util.HashMap;
import java.util.List;

import com.orderdtls.model.OrderDetails;


public interface OrderService {
	
	 String createOrder(String pPayload);
	 HashMap<String, Object> getOderDetails(int orderId);
	 List<HashMap> getAllOders();	 
	

}
